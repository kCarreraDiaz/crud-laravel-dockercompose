@extends('layouts.app')
@section('content')
<div class="container py-5">

    <p class="h1 text-center">Contactos</p>
    <hr>
    <div class="table-responsive">
        <table class="table" id="niveles">
            <thead>
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">apellido</th>
                    <th scope="col">Telefono</th>
                    <th scope="col">Dirección</th>
                    <th scope="col" style="text-align:center"><a href="{{ route('contacts.create') }}" class="h3"><i class="fas fa-plus-square text-success"></i></a></th>
                </tr>
            </thead>
          
            <tbody>
                @foreach ($contacts as $contact)
                    <tr>
                        <td>{{ $contact->nombre }}</td>
                        <td>{{ $contact->apellido }}</td>
                        <td>{{ $contact->telefono }}</td>
                        <td>{{ $contact->direccion }}</td>
                        <td class="d-flex justify-content-end">
                            <a href="{{ route('contacts.edit', $contact) }}" class="circulo waves-effect mr-2"
                                title="Editar">
                                <i class="fas fa-edit text-warning" aria-hidden="true"></i>
                            </a>
                            <form action="{{ route('contacts.destroy', $contact) }}" method="post">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="circulo waves-effect mr-2 border-0 bg-white" title="Eliminar"
                                    onclick="return confirm('¿Desea eliminar este registro?')">
                                    <i class="fas fa-trash-alt text-danger" aria-hidden="true"></i>
                                </button>
                            </form>
                        </>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @if ($contacts)
        <div class="mt-3 d-flex justify-content-end">
            {{ $contacts->links() }}
        </div>
        @endif
        
    </div>
</div>
@endsection