@extends('layouts.app')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <form action="{{ route('contacts.store') }}" method="post" enctype="multipart/form-data" onsubmit="return validar(this)">
        {{ csrf_field() }}  
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">Nuevo Contacto</h2>
            </div>
            <div class="panel-body" style="margin: 0px 200px 0px 0px">
                <div class="form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label for="name">Nombre</label>
                    <input type="text" name="nombre" value="{{ old('nombre') }}" class="form-control" maxlength="100" placeholder="Ingrese el nombre del contacto" required />
                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('apellido') ? ' has-error' : '' }}">
                    <label for="name">apellido</label>
                    <input type="text" name="apellido" value="{{ old('apellido') }}" class="form-control" maxlength="100" placeholder="Ingrese el Apellido del contacto" required />
                    @if ($errors->has('apellido'))
                        <span class="help-block">
                            <strong>{{ $errors->first('apellido') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('telefono') ? ' has-error' : '' }}">
                    <label for="name">telefono</label>
                    <input type="text" name="telefono" value="{{ old('telefono') }}" class="form-control" maxlength="100" placeholder="Ingrese el telefono del contacto" required />
                    @if ($errors->has('telefono'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telefono') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('direccion') ? ' has-error' : '' }}">
                    <label for="name">direccion</label>
                    <input type="text" name="direccion" value="{{ old('direccion') }}" class="form-control" maxlength="100" placeholder="Ingrese la direccion del contacto" required />
                    @if ($errors->has('direccion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('direccion') }}</strong>
                        </span>
                    @endif
                </div>
                
            </div>
  
            <div class="panel-footer">
                <input type="submit" class="btn btn-primary" value="Guardar" data-loading-text="Guardando..."/>
                <a href="{{  route('contacts.index') }}" class="btn btn-light">Cancelar</a>
            </div>
        </div>
    </form>
</main>
@endsection