@extends('layouts.app')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <form action="{{ route('contacts.update', $contact) }}" method="post" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {{ csrf_field() }}
        <div class="panel panel-default">
            <div class="panel-heading">
            <h4 class="border-bottom py-2 mb-4">Editar - Contacto</h4>
            </div>
            <div class="panel-body" style="margin: 0px 200px 0px 0px">
                <div class="form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label>Nombre</label>
                    <input type="text" name="nombre" value="{{ old('nombre', $contact->nombre) }}" class="form-control" placeholder="Ingrese el nombre" required />
                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('apellido') ? ' has-error' : '' }}">
                    <label>apellido</label>
                    <input type="text" name="apellido" value="{{ old('apellido', $contact->apellido) }}" class="form-control" placeholder="Ingrese el apellido" required />
                    @if ($errors->has('apellido'))
                        <span class="help-block">
                            <strong>{{ $errors->first('apellido') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('telefono') ? ' has-error' : '' }}">
                    <label>telefono</label>
                    <input type="text" name="telefono" value="{{ old('telefono', $contact->telefono) }}" class="form-control" placeholder="Ingrese el telefono" required />
                    @if ($errors->has('telefono'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telefono') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('direccion') ? ' has-error' : '' }}">
                    <label>direccion</label>
                    <input type="text" name="direccion" value="{{ old('direccion', $contact->direccion) }}" class="form-control" placeholder="Ingrese el direccion" required />
                    @if ($errors->has('direccion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('direccion') }}</strong>
                        </span>
                    @endif
                </div>
                
            </div>
            <div class="panel-footer">
                <input type="submit" class="btn btn-primary" value="Guardar" data-loading-text="Guardando..."/>
                <a href="{{ route('contacts.index') }}" class="btn btn-light">Cancelar</a>
            </div>
        </div>
    </form>
</main> 
@endsection